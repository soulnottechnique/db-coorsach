\connect markup;

CREATE OR REPLACE FUNCTION MarkupCount(user_name varchar) 
	RETURNS INTEGER
	AS $$
		SELECT count(*) 
		FROM MarkupUnit JOIN Account ON username = author
		WHERE username = user_name;
	$$
	LANGUAGE SQL;
