\connect markup;

DROP TABLE Account, MarkupUnit;
DROP USER common, linguist, admin;

\connect postgres;

DROP DATABASE markup;
