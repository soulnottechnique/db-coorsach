CREATE DATABASE markup;

CREATE USER common WITH PASSWORD '1234';
CREATE USER linguist WITH PASSWORD '1234';
CREATE USER admin WITH PASSWORD 'admin';

\connect markup;

CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE TABLE IF NOT EXISTS Account (
    username                varchar (256) NOT NULL,
    email                   varchar (256) NOT NULL,
    name                    varchar (256) NOT NULL,
    password                varchar (256) NOT NULL,
    role                    varchar (256) NULL,
    
    CONSTRAINT              ACCOUNT_USERNAME UNIQUE (username)
);

CREATE TABLE IF NOT EXISTS MarkupUnit (
    id                      varchar (256) PRIMARY KEY,
    author					varchar (256),
    wordform                text,
    category                text,
    morphology              text, 
    
    CONSTRAINT				MARKUP_AUTHOR FOREIGN KEY(author) 
    									  REFERENCES Account(username)
    									  ON DELETE CASCADE
);

GRANT SELECT ON MarkupUnit TO common, linguist;
GRANT INSERT ON MarkupUnit TO linguist;
GRANT ALL PRIVILEGES ON MarkupUnit, Account TO admin;

INSERT INTO Account (username, email, name, password, role) 
	VALUES ('admin', 'mail', 'egor', 'admin', 'ROLE_ADMIN');
	
\i func.sql
