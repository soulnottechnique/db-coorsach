#include "regwindow.h"
#include "ui_regwindow.h"

RegWindow::RegWindow(std::shared_ptr<Model> model, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::RegWindow),
    model(model)
{
    ui->setupUi(this);
}

RegWindow::~RegWindow()
{
    delete ui;
}

void RegWindow::closeEvent(QCloseEvent *event)
{
    emit closedSig();
}

void RegWindow::on_regButton_clicked()
{
    if (ui->loginLine->text().isEmpty() ||
        ui->mailLine->text().isEmpty() ||
        ui->nameLine->text().isEmpty() ||
        ui->passLine->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Поля не должны быть пусты");
        return;
    }

    try {
        std::string role = ui->lingvaCheck->isChecked() ? "ROLE_LINGUIST" : "ROLE_COMMON";
        Account acc(ui->loginLine->text().toStdString(),
                    ui->mailLine->text().toStdString(),
                    ui->nameLine->text().toStdString(),
                    ui->passLine->text().toStdString(),
                    role);

        model->execute(std::make_shared<AddAccount>(acc));
        QMessageBox::information(this, "Информация", "Регистрация успешна");
        this->close();

    } catch (std::exception &e) {QMessageBox::critical(this, "Ошибка", "Невозможно соединиться с сервером");}
}


void RegWindow::on_loginLine_textChanged(const QString &arg1)
{
    try {
        model->execute(std::make_shared<LoginExists>(arg1.toStdString()));
        if (model->getLoginAccept())
        {
            ui->statusBar->setText("Данный логин занят");
            ui->regButton->setEnabled(false);
        }
        else
        {
            ui->statusBar->setText("");
            ui->regButton->setEnabled(true);
        }
    } catch (std::exception &e) {QMessageBox::critical(this, "Ошибка", "Невозможно соединиться с сервером");}
}


