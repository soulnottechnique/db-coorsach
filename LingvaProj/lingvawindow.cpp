#include "lingvawindow.h"
#include "ui_lingvawindow.h"

lingvawindow::lingvawindow(std::shared_ptr<Model> model, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::lingvawindow),
    model(model)
{
    ui->setupUi(this);
    ui->treeWidget->setCurrentItem(ui->treeWidget->itemAt(0, 0));
}

void lingvawindow::closeEvent(QCloseEvent *event)
{
    emit closedSig();
}

lingvawindow::~lingvawindow()
{
    delete ui;
}

void lingvawindow::on_pushButton_clicked()
{
    try {
        model->execute(std::make_shared<FlushCache>());
        QMessageBox::information(this, "Информация", "Кэш очищен");
    } catch (std::exception &e) {QMessageBox::critical(this, "Ошибка", "Не удалось очистить кэш");}
}


void lingvawindow::on_checkBox_stateChanged(int arg1)
{
    try {
        model->execute(std::make_shared<EnableCache>(bool(arg1)));
        QMessageBox::information(this, "Информация", "Настройки кэширования успешно обновлены");
    } catch (std::exception &e) {QMessageBox::critical(this, "Ошибка", "Не удалось изменить настройки кэширования");}
}


void lingvawindow::on_searchButton_2_clicked()
{
    try {
        std::string wf = ui->lineEdit->text().toStdString();
        model->execute(std::make_shared<ReadMarkup>(wf));
        std::vector<MarkupUnit> mvec = model->getAllMarkup();
        if (mvec.empty())
            QMessageBox::information(this, "Информация", "В базе отсутствует данная словоформа");
        else
        {
            ui->MarkupTextBrowser->append(QString(acc->getLogin().c_str()) + ">>> " + ui->lineEdit->text() + "\n");
            for (auto m : mvec)
                ui->MarkupTextBrowser->append(QString(m.toMetaString().c_str()) + "\n");
        }
    } catch (std::exception &e) {QMessageBox::critical(this, "Ошибка", "Не удалось найти словоформу");}
}


void lingvawindow::on_searchAllButton_clicked()
{
    try {
        model->execute(std::make_shared<ReadAllMarkup>());
        std::vector<MarkupUnit> mvec = model->getAllMarkup();
        if (mvec.empty())
            QMessageBox::information(this, "Информация", "База пуста");
        else
        {
            ui->MarkupTextBrowser->append(QString(acc->getLogin().c_str()) + ">>> полный список\n");
            for (auto m : mvec)
                ui->MarkupTextBrowser->append(QString(m.toMetaString().c_str()) + "\n");
        }
    } catch (std::exception &e) {QMessageBox::critical(this, "Ошибка", "Не удалось прочитать словоформы");}
}

std::string lingvawindow::read_category()
{
    std::string text = ui->treeWidget->currentItem()->text(0).toStdString();
    QTreeWidgetItem *current = ui->treeWidget->currentItem();
    while (current->parent()) {
        current = current->parent();
        text = current->text(0).toStdString() + ": " + text;
    }

    return text;
}

void lingvawindow::add(QJsonObject &json)
{
    try {
        std::string text = this->read_category();

        MarkupUnit mu(ui->wordformLine->text().toStdString(),
                      acc->getLogin(),
                      Category(text),
                      Morphology(QJsonDocument(json).toJson().toStdString()));

        model->execute(std::make_shared<AddMarkup>(mu));

        QMessageBox::information(this, "Информация", "Разметка успешно добавлена");
    } catch (std::exception &e) {QMessageBox::critical(this, "Ошибка", "Не удалось добавить разметку");}
}

void lingvawindow::add_noun()
{
    QJsonObject json;

    if (ui->wordformLine->text().isEmpty() || ui->noun_infLine->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Поля не должны быть пусты");
        return;
    }

    json["Часть речи"] = "Существительное";

    json["Инфинитив"] = ui->noun_infLine->text();
    json["Одушевлённость"] = ui->noun_animatedRadio->isChecked() ? ui->noun_animatedRadio->text() : ui->noun_inanimatedRadio->text();
    json["Имя"] = ui->noun_commonRadio->isChecked() ? ui->noun_commonRadio->text() : ui->noun_ownRadio->text();
    json["Род"] = ui->noun_genuxBox->currentText();
    json["Склонение"] = ui->noun_declinationBox->currentText();
    json["Число"] = ui->noun_countBox->currentText();
    json["Падеж"] = ui->noun_caseBox->currentText();

    this->add(json);
}

void lingvawindow::add_adjective()
{
    QJsonObject json;

    if (ui->wordformLine->text().isEmpty() || ui->adj_infLine->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Поля не должны быть пусты");
        return;
    }

    json["Часть речи"] = "Прилагательное";

    json["Инфинитив"] = ui->adj_infLine->text();
    json["Род"] = ui->adj_genuxBox->currentText();
    json["Число"] = ui->adj_countBox->currentText();
    json["Падеж"] = ui->adj_caseBox->currentText();
    json["Разряд"] = ui->adj_const->currentText();

    if (ui->adj_formGroupBox->isEnabled())
        json["Форма"] = ui->adj_fullformRadio->isChecked() ? ui->adj_fullformRadio->text() : ui->adj_shortformRadio->text();

    if (ui->adj_cmpBox->isEnabled())
        json["Степень сравнения"] = ui->adj_cmpBox->currentText();

    this->add(json);
}

void lingvawindow::add_numeral()
{
    QJsonObject json;

    if (ui->wordformLine->text().isEmpty() || ui->num_infLine->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Поля не должны быть пусты");
        return;
    }

    json["Часть речи"] = "Числительное";

    json["Инфинитив"] = ui->num_infLine->text();
    json["Сложность"] = ui->num_simpleRadio->isChecked() ? ui->num_simpleRadio->text() : ui->num_complexRadio->text();
    json["Вид"] = ui->num_countRadio->isChecked() ? ui->num_countRadio->text() : ui->num_orderRadio->text();

    if (ui->num_classBox->isEnabled())
        json["Класс"] = ui->num_classBox->currentText();

    if (ui->num_genuxBox->currentText() != "-")
        json["Род"] = ui->num_genuxBox->currentText();
    if (ui->num_countBox->currentText() != "-")
        json["Число"] = ui->num_countBox->currentText();

    this->add(json);
}

void lingvawindow::add_pronoun()
{
    QJsonObject json;

    if (ui->wordformLine->text().isEmpty() || ui->pronoun_infLine->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Поля не должны быть пусты");
        return;
    }

    json["Часть речи"] = "Местоимение";

    json["Инфинитив"] = ui->pronoun_infLine->text();
    json["Падеж"] = ui->pronoun_caseBox->currentText();
    json["Разряд"] = ui->pronoun_categoryBox->currentText();

    if (ui->pronoun_faceBox->isEnabled())
        json["Лицо"] = ui->pronoun_faceBox->currentText();

    if (ui->pronoun_genuxBox->currentText() != "-")
        json["Род"] = ui->pronoun_genuxBox->currentText();
    if (ui->pronoun_countBox->currentText() != "-")
        json["Число"] = ui->pronoun_countBox->currentText();

    this->add(json);
}

void lingvawindow::add_verb()
{
    QJsonObject json;

    if (ui->wordformLine->text().isEmpty() || ui->verb_infLine->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Поля не должны быть пусты");
        return;
    }

    json["Часть речи"] = "Глагол";

    json["Вид"] = ui->verb_kindBox->currentText();
    json["Спряжение"] = ui->verb_conjugationBox->currentText();
    json["Переходность"] = ui->verb_transitivityBox->currentText();
    json["Наклонение"] = ui->verb_inclinationBox->currentText();
    json["Число"] = ui->verb_countBox->currentText();
    json["Инфинитив"] = ui->verb_infLine->text();

    if (ui->verb_faceBox->currentText() != "-")
        json["Лицо"] = ui->verb_faceBox->currentText();

    if (ui->verb_genuxBox->currentText() != "-")
        json["Род"] = ui->verb_genuxBox->currentText();

    if (ui->verb_timeBox->currentText() != "-")
        json["Время"] = ui->verb_timeBox->currentText();

    this->add(json);
}

void lingvawindow::add_participle()
{
    QJsonObject json;

    if (ui->wordformLine->text().isEmpty() || ui->participle_infLine->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Поля не должны быть пусты");
        return;
    }

    json["Часть речи"] = "Причастие";

    json["Число"] = ui->participle_countBox->currentText();
    json["Род"] = ui->participle_genuxBox->currentText();
    json["Инфинитив"] = ui->participle_infLine->text();
    json["Вид"] = ui->participle_kindBox->currentText();
    json["Залог"] = ui->participle_pledgeBox->currentText();
    json["Время"] = ui->participle_timeBox->currentText();

    if (ui->participle_caseBox->isEnabled())
        json["Падеж"] = ui->participle_caseBox->currentText();

    if (ui->participle_formGroupBox->isEnabled())
        json["Форма"] =ui->participle_fullformRadio->isChecked() ? ui->participle_fullformRadio->text() : ui->participle_shortformRadio->text();

    this->add(json);
}

void lingvawindow::add_adv_participle()
{
    QJsonObject json;

    if (ui->wordformLine->text().isEmpty() || ui->adv_participle_infLine->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Поля не должны быть пусты");
        return;
    }

    json["Часть речи"] = "Деепричастие";

    json["Инфинитив"] = ui->adv_participle_infLine->text();
    json["Вид"] = ui->adv_participle_kindBox->currentText();
    json["Возвратность"] = ui->adv_participle_returnableBox->currentText();
    json["Переходность"] = ui->adv_participle_transitivityBox->currentText();

    this->add(json);
}

void lingvawindow::add_adverb()
{
    QJsonObject json;

    if (ui->wordformLine->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Поля не должны быть пусты");
        return;
    }

    json["Часть речи"] = "Наречие";

    json["Группа по значению"] = ui->adverb_groupBox->currentText();

    if (ui->adverb_cmpBox->currentText() != "-")
        json["Степень сравнения"] = ui->adverb_cmpBox->currentText();

    this->add(json);
}

void lingvawindow::add_preposition()
{
    QJsonObject json;

    if (ui->wordformLine->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Поля не должны быть пусты");
        return;
    }

    json["Часть речи"] = "Предлог";

    json["Производность"] = ui->preposition_derivativeBox->currentText();

    this->add(json);
}

void lingvawindow::add_conjunction()
{
    QJsonObject json;

    if (ui->wordformLine->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Поля не должны быть пусты");
        return;
    }

    json["Часть речи"] = "Союз";

    json["Тип связи"] = ui->conjunction_linkBox->currentText();

    this->add(json);
}

void lingvawindow::add_particle()
{
    QJsonObject json;

    if (ui->wordformLine->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Поля не должны быть пусты");
        return;
    }

    json["Часть речи"] = "Частица";

    json["Разряд"] = ui->particle_classBox->currentText();

    this->add(json);
}

void lingvawindow::add_interjection()
{
    QJsonObject json;

    if (ui->wordformLine->text().isEmpty())
    {
        QMessageBox::critical(this, "Ошибка", "Поля не должны быть пусты");
        return;
    }

    json["Часть речи"] = "Междометие";

    this->add(json);
}

void lingvawindow::on_addButton_clicked()
{
    switch (ui->morph->currentIndex()) {
    case 0:
        this->add_noun();
        break;
    case 1:
        this->add_adjective();
        break;
    case 2:
        this->add_numeral();
        break;
    case 3:
        this->add_pronoun();
        break;
    case 4:
        this->add_verb();
        break;
    case 5:
        this->add_participle();
        break;
    case 6:
        this->add_adv_participle();
        break;
    case 7:
        this->add_adverb();
        break;
    case 8:
        this->add_preposition();
        break;
    case 9:
        this->add_conjunction();
        break;
    case 10:
        this->add_particle();
        break;
    case 11:
        this->add_interjection();
        break;
    default:
        break;
    }
}


void lingvawindow::on_adj_const_currentIndexChanged(int index)
{

    ui->adj_cmpBox->setEnabled(!index);
    ui->adj_formGroupBox->setEnabled(!index);
}


void lingvawindow::on_num_countRadio_toggled(bool checked)
{

    ui->num_classBox->setEnabled(checked);
}


void lingvawindow::on_pronoun_categoryBox_currentIndexChanged(int index)
{
    ui->pronoun_faceBox->setEnabled(!index);
}



void lingvawindow::on_participle_pledgeBox_currentIndexChanged(int index)
{
    ui->participle_formGroupBox->setEnabled(!index);
    ui->participle_caseBox->setEnabled(ui->participle_fullformRadio->isChecked() && !index);
}


void lingvawindow::on_participle_fullformRadio_toggled(bool checked)
{
    ui->participle_caseBox->setEnabled(checked);
}


void lingvawindow::on_read_clearButton_clicked()
{
    ui->MarkupTextBrowser->clear();
}

