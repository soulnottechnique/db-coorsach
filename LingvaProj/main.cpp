#include "loginwindow.h"

#include <QApplication>
#include <memory>

#include "modelset.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    std::shared_ptr<ModelSet> mset = std::make_shared<ModelSet>("../conf/conf");

    LoginWindow w(mset);
    w.show();

    return a.exec();
}
