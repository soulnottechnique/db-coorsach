#ifndef MODELSET_H
#define MODELSET_H

#include "src/core/model/model.h"
#include "src/core/config/config.h"

class ModelSet {
public:
    ModelSet(std::string filename);

    std::shared_ptr<Model> getAdminModel() {return adminModel; }
    std::shared_ptr<Model> getLingvaModel() {return lingvaModel; }
    std::shared_ptr<Model> getCommonModel() {return commonModel; }

private:
    std::shared_ptr<Model> adminModel, lingvaModel, commonModel;

};

#endif // MODELSET_H
