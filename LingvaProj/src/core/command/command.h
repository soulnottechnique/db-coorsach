#pragma once

#include <memory>
#include "../config/baseconfig.h"
#include "../facade/facade.h"

class Command {
public:
    Command() = default;
    virtual ~Command() = default;

    virtual void execute() = 0;
    void setFacade(std::shared_ptr<BaseFacade> facade);

protected:
    std::shared_ptr<BaseFacade> facade;
};

class AddAccount : public Command {
public:
    AddAccount(Account &account);
    virtual ~AddAccount() = default;

    virtual void execute() override;

private:
    Account account;
};

class AddMarkup : public Command {
public:
    AddMarkup(MarkupUnit &markup);
    virtual ~AddMarkup() = default;

    virtual void execute() override;

private:
    MarkupUnit markup;
};

class LoginAccept : public Command {
public:
    LoginAccept(std::string login, std::string password);
    virtual ~LoginAccept() = default;

    virtual void execute() override;

private:
    std::string login, password;
};

class LoginExists : public Command {
public:
    LoginExists(std::string login);
    virtual ~LoginExists() = default;

    virtual void execute() override;

private:
    std::string login;
};

class ReadAllAccounts : public Command {
public:
    ReadAllAccounts() = default;
    virtual ~ReadAllAccounts() = default;

    virtual void execute() override;
};

class ReadAllMarkup : public Command {
public:
    ReadAllMarkup() = default;
    virtual ~ReadAllMarkup() = default;

    virtual void execute() override;
};

class ReadMarkup : public Command {
public:
    ReadMarkup(std::string wordform) : wordform(wordform) {}
    virtual ~ReadMarkup() = default;

    virtual void execute() override;

private:
    std::string wordform;
};

class FlushCache : public Command {
public:
    FlushCache() = default;
    virtual ~FlushCache() = default;

    virtual void execute() override;
};

class EnableCache : public Command {
public:
    EnableCache(bool status) : status(status) {}
    virtual ~EnableCache() = default;

    virtual void execute() override;

private:
    bool status;
};

class ReadAccount : public Command {
public:
    ReadAccount(std::string login) : login(login) {}
    virtual ~ReadAccount() = default;

    virtual void execute() override;

private:
    std::string login;
};

class DeleteAccount : public Command {
public:
    DeleteAccount(std::string login) : login(login) {}
    virtual ~DeleteAccount() = default;

    virtual void execute() override;

private:
    std::string login;
};