#include "../command/command.h"

void Command::setFacade(std::shared_ptr<BaseFacade> facade)
{
    this->facade = facade;
}


AddAccount::AddAccount(Account &account) :
    account(account) {}

AddMarkup::AddMarkup(MarkupUnit &markup):
    markup(markup) {}

LoginAccept::LoginAccept(std::string login, std::string password):
    login(login), password(password) {}

LoginExists::LoginExists(std::string login) :
    login(login) {}


void AddAccount::execute()
{
    facade->addAccount(account);
}

void AddMarkup::execute()
{
    facade->addMarkup(markup);
}

void LoginAccept::execute()
{
    facade->loginAccept(login, password);
}

void LoginExists::execute()
{
    facade->loginExists(login);
}

void ReadAllAccounts::execute()
{
    facade->readAllAccounts();
}

void ReadAllMarkup::execute()
{
    facade->readAllMakup();
}

void ReadAccount::execute()
{
    facade->readAccount(login);
}

void DeleteAccount::execute()
{
    facade->deleteAccount(login);
}

void ReadMarkup::execute()
{
    facade->readMarkup(wordform);
}

void FlushCache::execute()
{
    facade->flushCache();
}

void EnableCache::execute()
{
    facade->enableCache(status);
}