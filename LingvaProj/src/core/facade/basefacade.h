#pragma once

#include <memory>
#include "../config/baseconfig.h"
#include "../sysent/account.h"
#include "../sysent/markupunit.h"

#include "../../da/repository/accountrepository.h"
#include "../../da/repository/markuprepository.h"
#include "../../da/cache/cache.h"

class BaseFacade {
public:
    BaseFacade() = default;
    virtual ~BaseFacade() = default;

    virtual void addAccount(Account &account) = 0;
    virtual void addMarkup(MarkupUnit &markup) = 0;
    virtual void addMarkup(std::vector<MarkupUnit> mvec) = 0;
    virtual void loginAccept(std::string login, std::string password) = 0;
    virtual void readAllAccounts() = 0;
    virtual void readAllMakup() = 0;
    virtual void readAccount(std::string login) = 0;
    virtual void deleteAccount(std::string login) = 0;
    virtual void readMarkup(std::string wordform) = 0;
    virtual void loginExists(std::string login) = 0;

    bool getLoginAccept() {return accept;}
    std::vector<Account> getAllAccounts() {return allAccounts;}
    std::vector<MarkupUnit> getAllMarkup() {return allMarkup;}
    Account getAccount() {return account;}
    bool emptyResponse() {return emptyResp;}

    void enableCache(bool status) {this->flushCache(); cacheEnabled = status;}
    virtual void flushCache() = 0;

protected:
    bool accept, cacheEnabled = true, emptyResp;
    std::vector<Account> allAccounts;
    std::vector<MarkupUnit> allMarkup;
    Account account;
};
