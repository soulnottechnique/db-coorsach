#include "../facade/facade.h"

Facade::Facade(std::shared_ptr<BaseConfig> conf)
{
    std::shared_ptr<AccountRepositoryCreator> accountCreator = conf->getAccountRepositoryCreator();
    accountRepository = accountCreator->create();

    std::shared_ptr<MarkupRepositoryCreator> markupCreator = conf->getMarkupRepositoryCreator();
    markupRepository = markupCreator->create();

    std::shared_ptr<MarkupCacheCreator> markupCacheCreator = conf->getMarkupCacheCreator();
    markupCache = markupCacheCreator->create();
}

Facade::~Facade()
{
    this->flushCache();
}

void Facade::addAccount(Account &account)
{
    accountRepository->create(account);
}

void Facade::addMarkup(MarkupUnit &markup)
{
    if (cacheEnabled)
        markupCache->put(markup);
    else
        markupRepository->create(markup);
}

void Facade::flushCache()
{
    if (!markupCache->empty())
    {
        std::vector<MarkupUnit> mvec = markupCache->get();
        markupRepository->create(mvec);
    }
}

void Facade::loginAccept(std::string login, std::string password)
{
    try {
    Account acc = accountRepository->read(login);
    accept = (acc.getPassword() == password);
    account = acc;
    } catch (EmptyResponse &e) {accept = false;}
}

void Facade::readAllAccounts()
{
    allAccounts = accountRepository->readAll(); 
}

void Facade::readAllMakup()
{
    this->flushCache();
    allMarkup = markupRepository->readAll();
}

void Facade::readAccount(std::string login)
{
    try {
    account = accountRepository->read(login);
    emptyResp = false;
    } catch (EmptyResponse &e) {emptyResp = true;}
}

void Facade::deleteAccount(std::string login)
{
    try {
    accountRepository->delete_(login);
    emptyResp = false;
    } catch (BadDelete &e) {emptyResp = true;}
}

void Facade::readMarkup(std::string wordform)
{
    this->flushCache();
    allMarkup = markupRepository->read(wordform);
}

void Facade::addMarkup(std::vector<MarkupUnit> mvec)
{
    markupRepository->create(mvec);
}

void Facade::loginExists(std::string login)
{
    try {
    Account acc = accountRepository->read(login);
    accept = true;
    } catch (EmptyResponse &e) {accept = false;}
}
