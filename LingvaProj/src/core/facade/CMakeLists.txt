cmake_minimum_required(VERSION 3.8)

project(facade)

add_library(facade SHARED 
    basefacade.h
    facade.h
    facade.cpp
)

set_target_properties(facade PROPERTIES LINKER_LANGUAGE CXX)