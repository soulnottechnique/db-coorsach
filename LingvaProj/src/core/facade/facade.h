#pragma once

#include <memory>
#include "../facade/basefacade.h"

class Facade : public BaseFacade {
public:
    Facade(std::shared_ptr<BaseConfig> conf);
    virtual ~Facade();

    virtual void addAccount(Account &account) override;
    virtual void addMarkup(MarkupUnit &markup) override;
    virtual void addMarkup(std::vector<MarkupUnit> mvec) override;
    virtual void loginAccept(std::string login, std::string password) override;
    virtual void readAllAccounts() override;
    virtual void readAllMakup() override;
    virtual void readAccount(std::string login) override;
    virtual void deleteAccount(std::string login) override;
    virtual void readMarkup(std::string wordform) override;
    virtual void loginExists(std::string login) override;
    
    virtual void flushCache() override;

private:
    std::shared_ptr<AccountRepository> accountRepository;
    std::shared_ptr<MarkupRepository> markupRepository;
    std::shared_ptr<MarkupCache> markupCache;
};