#include "../config/registrator.h"

template<>
Registrator<class AccountRepositoryCreator>::Registrator(std::string connectionString)
{
    register_("TestAccount", std::make_shared<TestAccountRepositoryCreator>(connectionString));
    register_("PgAccount", std::make_shared<PgAccountRepositoryCreator>(connectionString));
}

template<>
Registrator<class MarkupRepositoryCreator>::Registrator(std::string connectionString)
{
    register_("TestMarkup", std::make_shared<TestMarkupRepositoryCreator>(connectionString));
    register_("PgMarkup", std::make_shared<PgMarkupRepositoryCreator>(connectionString));
}

template<>
Registrator<class MarkupCacheCreator>::Registrator(std::string port)
{
    register_("RedisCache", std::make_shared<RedisMarkupCacheCreator>(port));
}