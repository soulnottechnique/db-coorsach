#pragma once

#include <memory>
#include "../config/registrator.h"

class BaseConfig {
public:
    BaseConfig() = default;
    virtual ~BaseConfig() = default;

    virtual std::shared_ptr<AccountRepositoryCreator> getAccountRepositoryCreator() = 0;
    virtual std::shared_ptr<MarkupRepositoryCreator> getMarkupRepositoryCreator() = 0;
    virtual std::shared_ptr<MarkupCacheCreator> getMarkupCacheCreator() = 0;
    virtual std::string getConnectionString() = 0;
};