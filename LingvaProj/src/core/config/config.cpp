#include "../config/config.h"

Config::Config(std::string filename, std::string role)
{
    std::string acs, lcs, ccs;

    std::ifstream in;
    in.open(filename);
    std::getline(in, accountName);
    std::getline(in, markupName);
    std::getline(in, acs);
    std::getline(in, lcs);
    std::getline(in, ccs);
    std::getline(in, cacheName);
    std::getline(in, cachePort);
    in.close();

    if (role == "ROLE_ADMIN" || role == "")
        connectionString = acs;
    else if (role == "ROLE_COMMON")
        connectionString = ccs;
    else
        connectionString = lcs;

    accountRepository = std::make_shared<Registrator<AccountRepositoryCreator>>(connectionString);
    markupRepository = std::make_shared<Registrator<MarkupRepositoryCreator>>(connectionString);
    markupCache = std::make_shared<Registrator<MarkupCacheCreator>>(cachePort);
}

std::shared_ptr<AccountRepositoryCreator> Config::getAccountRepositoryCreator()
{
    return accountRepository->getCreator(accountName);
}

std::shared_ptr<MarkupRepositoryCreator> Config::getMarkupRepositoryCreator()
{
    return markupRepository->getCreator(markupName);
}

std::shared_ptr<MarkupCacheCreator> Config::getMarkupCacheCreator()
{
    return markupCache->getCreator(cacheName);
}

std::string Config::getConnectionString()
{
    return connectionString;
}
