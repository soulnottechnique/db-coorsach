#pragma once

#include <string>
#include <fstream>
#include "../config/baseconfig.h"
#include "../config/registrator.h"
#include "../../da/repository/accountrepository.h"
#include "../../da/repository/markuprepository.h"
#include "../../da/cache/cache.h"

class Config : public BaseConfig {
public:
    Config(std::string filename, std::string role = "");
    virtual ~Config() = default;

    virtual std::shared_ptr<AccountRepositoryCreator> getAccountRepositoryCreator() override;
    virtual std::shared_ptr<MarkupRepositoryCreator> getMarkupRepositoryCreator() override;
    virtual std::shared_ptr<MarkupCacheCreator> getMarkupCacheCreator() override;
    virtual std::string getConnectionString() override;

private:
    std::shared_ptr<Registrator<AccountRepositoryCreator>> accountRepository;
    std::shared_ptr<Registrator<MarkupRepositoryCreator>> markupRepository;
    std::shared_ptr<Registrator<MarkupCacheCreator>> markupCache;

    std::string accountName, markupName, connectionString, cacheName, cachePort;
};
