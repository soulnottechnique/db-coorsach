#pragma once

#include <map>
#include <string>
#include <memory>

#include "../../da/repository/testaccountrepository.h"
#include "../../da/repository/testmarkuprepository.h"

#include "../../da/repository/pgaccountrepository.h"
#include "../../da/repository/pgmarkuprepository.h"

#include "../../da/cache/rediscache.h"


template<class T>
class Registrator {
public:
    Registrator(std::string connectionString);
    ~Registrator() = default;

    void register_(std::string name, std::shared_ptr<T> creator);
    std::shared_ptr<T> getCreator(std::string name);

private:
    std::map<std::string, std::shared_ptr<T>> map;
};

#include "../config/registrator.hpp"