template<class T>
void Registrator<T>::register_(std::string name, std::shared_ptr<T> creator)
{
    map[name] = creator;
}

template<class T>
std::shared_ptr<T> Registrator<T>::getCreator(std::string name)
{
    return map.at(name);
}