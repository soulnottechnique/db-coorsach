cmake_minimum_required(VERSION 3.8)

project(core)	

add_subdirectory(command)
add_subdirectory(facade)
add_subdirectory(sysent)
add_subdirectory(config)
add_subdirectory(model)

add_library(core INTERFACE)
target_link_libraries(core
    INTERFACE
    model
    command
    facade
    sysent
    config
)