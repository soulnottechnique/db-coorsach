#pragma once

#include "../config/config.h"
#include "../facade/facade.h"
#include "../command/command.h"

class Model
{
public:
    Model(std::shared_ptr<BaseConfig> conf);
    ~Model() = default;

    void execute(std::shared_ptr<Command> cmd);

    bool emptyResponse();
    bool getLoginAccept();
    std::vector<Account> getAllAccounts();
    std::vector<MarkupUnit> getAllMarkup();
    Account getAccount();

private:
    std::shared_ptr<BaseFacade> facade;
};
