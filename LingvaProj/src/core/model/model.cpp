#include "model.h"

Model::Model(std::shared_ptr<BaseConfig> conf) : facade(std::make_shared<Facade>(conf)) {}

void Model::execute(std::shared_ptr<Command> cmd)
{
    cmd->setFacade(facade);
    cmd->execute();
}

bool Model::getLoginAccept()
{
    return facade->getLoginAccept();
}

std::vector<Account> Model::getAllAccounts()
{
    return facade->getAllAccounts();
}

std::vector<MarkupUnit> Model::getAllMarkup()
{
    return facade->getAllMarkup();
}

Account Model::getAccount()
{
    return facade->getAccount();
}

bool Model::emptyResponse()
{
    return facade->emptyResponse();
}
