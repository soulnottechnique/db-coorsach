#pragma once

#include <string>

typedef enum {
    COMMON,
    LINGUIST,
    ADMIN
} role_t;

const std::string rolename[] = {std::string("ROLE_COMMON"), 
    std::string("ROLE_LINGUIST"), 
    std::string("ROLE_ADMIN")};