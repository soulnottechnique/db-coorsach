cmake_minimum_required(VERSION 3.8)

project(sysent)

add_library(sysent SHARED 
    account.cpp
    account.h
    category.cpp
    category.h
    markupunit.cpp
    markupunit.h
    morphology.cpp
    morphology.h
    role.h
)

set_target_properties(sysent PROPERTIES LINKER_LANGUAGE CXX)
    