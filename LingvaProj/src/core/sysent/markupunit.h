#pragma once

#include <string>
#include <vector>
#include "../sysent/category.h"
#include "../sysent/morphology.h"

class MarkupUnit {
public:
    MarkupUnit(std::string, std::string, Category, Morphology);
    MarkupUnit(std::string);
    ~MarkupUnit() = default;

    std::string toString();
    std::string toMetaString();

private:
    std::string wordform, author_id;
    Category category;
    Morphology morphology;
};
