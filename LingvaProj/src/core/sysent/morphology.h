#pragma once

#include <string>

class Morphology {
public:
    Morphology() = default;
    Morphology(std::string);
    ~Morphology() = default;

    std::string toStrinig() {return morphology;}
private:
    std::string morphology;
};