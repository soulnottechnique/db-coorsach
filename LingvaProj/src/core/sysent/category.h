#pragma once

#include <string>

class Category {
public:
    Category() = default;
    Category(std::string);
    ~Category() = default;

    std::string toString() {return category;}

private:
    std::string category;
};