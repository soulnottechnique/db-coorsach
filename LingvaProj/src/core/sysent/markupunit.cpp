#include "../sysent/markupunit.h"

MarkupUnit::MarkupUnit(std::string wf, std::string author, Category cat, Morphology morph) :
    wordform(wf), author_id(author), category(cat), morphology(morph) {}

static std::vector<std::string> split (std::string s, std::string delimiter) 
{
    size_t pos_start = 0, pos_end, delim_len = delimiter.length();
    std::string token;
    std::vector<std::string> res;

    while ((pos_end = s.find (delimiter, pos_start)) != std::string::npos) 
    {
        token = s.substr (pos_start, pos_end - pos_start);
        pos_start = pos_end + delim_len;
        res.push_back (token);
    }

    res.push_back (s.substr (pos_start));
    return res;
}

MarkupUnit::MarkupUnit(std::string all)
{
    std::vector<std::string> res = split(all, "', '");
    wordform = res[0].substr(1, res[0].size() - 1);
    author_id = res[1];
    category = Category(res[2]);
    morphology = Morphology(res[3].substr(0, res[3].size() - 1));
}

std::string MarkupUnit::toString()
{
    return std::string("'" + wordform + "', '" + author_id + "', '" + category.toString() + "', '" + morphology.toStrinig() + "'");
}

std::string MarkupUnit::toMetaString()
{
    return std::string("Словоформа = '" + wordform + "', автор = '" + author_id + "', категория='" + category.toString() + "', морфологические признаки='" + morphology.toStrinig() + "'");
}
