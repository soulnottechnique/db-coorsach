#include "../sysent/account.h"

Account::Account() : 
    login("Test"), mail("Test"), name("Test"), password("Test"), role("ROLE_COMMON") {}

Account::Account(std::string login, std::string mail, std::string name, std::string password, std::string role, int count) :
    login(login), mail(mail), name(name), password(password), role(role), count(count) {}

std::string Account::getPassword()
{
    return password;
}

std::string Account::toString()
{
    return std::string("'" + login + "', '" + mail + "', '" + name + "', '" + password + "', '" + role + "'");
}

std::string Account::toMetaString()
{
    return std::string("Логин = '" + login + "', почта = '" + mail + "', имя = '" +
        name + "', пароль = '" + password + "', роль = '" + role + "', число записей = '" + std::to_string(count) + "'");
}
