#pragma once

#include <string>

class Account {
public:
    Account();
    Account(std::string login, std::string mail, std::string name, std::string password, std::string role, int count = 0);
    ~Account() = default;

    std::string getPassword();
    std::string toString();
    std::string toMetaString();
    std::string getRole() {return role;}
    std::string getLogin() {return login;}

private:
    std::string name, mail, login, password;
    std::string role;
    int count;
};
