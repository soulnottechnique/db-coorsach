#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include <pqxx/pqxx>
#include "../repository/markuprepository.h"

class PgMarkupRepository : public MarkupRepository {
public:
    PgMarkupRepository(std::string connectionString);
    virtual ~PgMarkupRepository() = default;

    virtual void create(MarkupUnit &markup) override;
    virtual void create(std::vector<MarkupUnit> mvec) override;
    virtual std::vector<MarkupUnit> read(std::string wordform) override;
    virtual std::vector<MarkupUnit> readAll() override;
    virtual void update(std::string wordform, MarkupUnit &markup) override;
    virtual void delete_(std::string wordform) override;

private:
    std::shared_ptr<pqxx::connection> connection;
};

class PgMarkupRepositoryCreator : public MarkupRepositoryCreator {
public:
    PgMarkupRepositoryCreator(std::string connectionString) : MarkupRepositoryCreator(connectionString) {}
    virtual ~PgMarkupRepositoryCreator() = default;

    virtual std::shared_ptr<MarkupRepository> create() override;
};