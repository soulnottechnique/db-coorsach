#include "../repository/pgaccountrepository.h"

PgAccountRepository::PgAccountRepository(std::string connectionString) : AccountRepository(connectionString)
{
    try {
        connection = std::make_shared<pqxx::connection>(connectionString);
    } catch (pqxx::broken_connection &e) {
        connection = nullptr;
    }
}

void PgAccountRepository::create(Account &account)
{
    if (connection && connection->is_open())
    {
        pqxx::work txn{*connection};  

        std::string query(
        "insert into account(username, email, name, password, role) "
        "values (" + account.toString() + ");");

        try {
        txn.exec(query);
        } catch (...) {BadInsert("PgAccountRepository::create exception during insert");}
        txn.commit();
    }
    else
        throw CloseConnection("PgMarkupRepository::create connection isn't open");
}

Account PgAccountRepository::read(std::string login)
{
    if (connection && connection->is_open())
    {
        pqxx::work txn{*connection};  

        std::string query(
        "select * from account "
        "where username = '" + login + "';");

        pqxx::result res = txn.exec(query);

        if (!res.size())
            throw EmptyResponse("PgAccountRepository::read query is empty");

        pqxx::result cnt = txn.exec("select markupcount('" + login + "');");

        Account acc(login, 
            res.begin()["email"].as<std::string>(),
            res.begin()["name"].as<std::string>(), 
            res.begin()["password"].as<std::string>(), 
            res.begin()["role"].as<std::string>(),
            cnt.begin()["markupcount"].as<int>());

        return acc;
    }
    else
        throw CloseConnection("PgMarkupRepository::read connection isn't open");
}

std::vector<Account> PgAccountRepository::readAll()
{
    if (connection && connection->is_open())
    {
        pqxx::work txn{*connection};  

        std::string query("select username, email, name, password, role from account;");

        pqxx::result res = txn.exec(query);

        std::vector<Account> acc;
        for (auto row : res)
        {
            std::string uname = row["username"].as<std::string>();
            pqxx::result cnt = txn.exec("select markupcount('" + uname + "');");
            acc.push_back(Account(
                uname,
                row["email"].as<std::string>(),
                row["name"].as<std::string>(), 
                row["password"].as<std::string>(), 
                row["role"].as<std::string>(),
                cnt.begin()["markupcount"].as<int>()));
        }

        return acc;
    }
    else
        throw CloseConnection("PgMarkupRepository::readAll connection isn't open");
}

void PgAccountRepository::update(std::string login, Account &account)
{
    if (connection && connection->is_open())
    {
        pqxx::work txn{*connection};  

        std::string query(
        "update account set " + account.toMetaString() + 
        "where username = '" + login + "';");

        try {
        txn.exec(query);
        }
        catch (...) {throw BadUpdate("PgAccountRepository::update exception during excecution time");}

        txn.commit();
    }
    else
        throw CloseConnection("PgMarkupRepository::update connection isn't open");
}

void PgAccountRepository::delete_(std::string login)
{
    if (connection && connection->is_open())
    {
        pqxx::work txn{*connection};  

        std::string query(
        "delete from account "
        "where username = '" + login + "';");
        try {
        txn.exec(query);
        } catch (...) {throw BadDelete("PgAccountRepository::delete_ exception during delete");}
        txn.commit();
    }
    else
        throw CloseConnection("PgMarkupRepository::delete_ connection isn't open");
}

std::shared_ptr<AccountRepository> PgAccountRepositoryCreator::create()
{
    return std::make_shared<PgAccountRepository>(connectionString);
}
