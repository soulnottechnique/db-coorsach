#pragma once

#include <iostream>
#include <vector>
#include <memory>
#include "../repository/markuprepository.h"

class TestMarkupRepository : public MarkupRepository {
public:
    TestMarkupRepository(std::string connectionString) : MarkupRepository(connectionString) {}
    virtual ~TestMarkupRepository() = default;

    virtual void create(MarkupUnit &markup) override;
    virtual void create(std::vector<MarkupUnit> mvec) override;
    virtual std::vector<MarkupUnit> read(std::string wordform) override;
    virtual std::vector<MarkupUnit> readAll() override;
    virtual void update(std::string wordform, MarkupUnit &markup) override;
    virtual void delete_(std::string wordform) override;
};

class TestMarkupRepositoryCreator : public MarkupRepositoryCreator {
public:
    TestMarkupRepositoryCreator(std::string connectionString) : MarkupRepositoryCreator(connectionString) {}
    virtual ~TestMarkupRepositoryCreator() = default;

    virtual std::shared_ptr<MarkupRepository> create() override;
};