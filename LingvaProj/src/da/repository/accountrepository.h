#pragma once

#include <vector>
#include "../../core/sysent/account.h"
#include "../exceptions/AccessException.hpp"

class AccountRepository {
public:
    AccountRepository(std::string connectionString) : connectionString(connectionString) {}
    virtual ~AccountRepository() = default;

    virtual void create(Account &account) = 0;
    virtual Account read(std::string login) = 0;
    virtual std::vector<Account> readAll() = 0;
    virtual void update(std::string login, Account &account) = 0;
    virtual void delete_(std::string login) = 0;

protected:
    std::string connectionString;
};

class AccountRepositoryCreator {
public:
    AccountRepositoryCreator(std::string connectionString) : connectionString(connectionString) {}
    virtual ~AccountRepositoryCreator() = default;

    virtual std::shared_ptr<AccountRepository> create() = 0;

protected:
    std::string connectionString;
};