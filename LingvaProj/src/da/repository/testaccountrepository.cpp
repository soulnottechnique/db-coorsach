#include "../repository/testaccountrepository.h"

void TestAccountRepository::create(Account &account)
{
    std::cout << "TestAccountRepository::create" << std::endl;
}

Account TestAccountRepository::read(std::string login)
{
    std::cout << "TestAccountRepository::read, login = " << login << std::endl;
    return Account();
}

std::vector<Account> TestAccountRepository::readAll()
{
    std::cout << "TestAccountRepository::readAll" << std::endl;
    return std::vector<Account>();
}

void TestAccountRepository::update(std::string login, Account &account)
{
    std::cout << "TestAccountRepository::update, login = " << login << std::endl;
}

void TestAccountRepository::delete_(std::string login)
{
    std::cout << "TestAccountRepository::delete_, login = " << login << std::endl;
}

std::shared_ptr<AccountRepository> TestAccountRepositoryCreator::create()
{
    return std::make_shared<TestAccountRepository>(connectionString);
}