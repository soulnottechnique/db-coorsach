#pragma once

#include <iostream>
#include <memory>
#include "../repository/accountrepository.h"

class TestAccountRepository : public AccountRepository {
public:
    TestAccountRepository(std::string connectionString) : AccountRepository(connectionString) {}
    virtual ~TestAccountRepository() = default;

    virtual void create(Account &account) override;
    virtual Account read(std::string login) override;
    virtual std::vector<Account> readAll() override;
    virtual void update(std::string login, Account &account) override;
    virtual void delete_(std::string login) override;
};

class TestAccountRepositoryCreator : public AccountRepositoryCreator{
public:
    TestAccountRepositoryCreator(std::string connectionString) : AccountRepositoryCreator(connectionString) {}
    virtual ~TestAccountRepositoryCreator() = default;

    virtual std::shared_ptr<AccountRepository> create() override;
};