#include "../repository/pgmarkuprepository.h"

PgMarkupRepository::PgMarkupRepository(std::string connectionString) : MarkupRepository(connectionString)
{
    try {
    connection = std::make_shared<pqxx::connection>(connectionString);
    } catch (pqxx::broken_connection &e) {
        connection = nullptr;
    }
}

void PgMarkupRepository::create(MarkupUnit &markup)
{
    if (connection && connection->is_open())
    {
        pqxx::work txn{*connection};  
        std::string query = "insert into markupunit(id, wordform, author, category, morphology) "
        "values (uuid_generate_v4(), " + markup.toString() + ");";

        try {
        txn.exec(query);
        } catch (...) {throw BadInsert("PgMarkupRepository::create exception during insert");}
        txn.commit();
    }
    else
        throw CloseConnection("PgMarkupRepository::create connection isn't open");
}

void PgMarkupRepository::create(std::vector<MarkupUnit> mvec)
{
    if (connection && connection->is_open())
    {
        pqxx::work txn{*connection};  
        std::string query = "insert into markupunit(id, wordform, author, category, morphology) values ";

        for (auto m : mvec)
            query +=  "(uuid_generate_v4(), " + m.toString() + "), \n";

        query = query.substr(0, query.size() - 3) + ";";

        try {
        txn.exec(query);
        } catch (...) {throw BadInsert("PgMarkupRepository::create exception during insert");}
        txn.commit();
    }
    else
        throw CloseConnection("PgMarkupRepository::create connection isn't open");
}

std::vector<MarkupUnit> PgMarkupRepository::read(std::string wordform)
{
    if (connection && connection->is_open())
    {
        pqxx::work txn{*connection};  

        std::string query(
        "select * from markupunit "
        "where wordform = '" + wordform + "';");

        pqxx::result res = txn.exec(query);

        std::vector<MarkupUnit> acc;
        for (auto row : res) acc.push_back(

        MarkupUnit( 
            row["wordform"].as<std::string>(),
            row["author"].as<std::string>(),
            Category(row["category"].as<std::string>()), 
            Morphology(row["morphology"].as<std::string>())
            )
            
        );

        return acc;
    }
    else
        throw CloseConnection("PgMarkupRepository::read connection isn't open");
}

std::vector<MarkupUnit> PgMarkupRepository::readAll()
{
    if (connection && connection->is_open())
    {
        pqxx::work txn{*connection};  

        std::string query(
        "select * from markupunit;");

        pqxx::result res = txn.exec(query);

        std::vector<MarkupUnit> acc;
        for (auto row : res) acc.push_back(

        MarkupUnit( 
            row["wordform"].as<std::string>(),
            row["author"].as<std::string>(),
            Category(row["category"].as<std::string>()), 
            Morphology(row["morphology"].as<std::string>())
            )
            
        );

        return acc;
    }
    else
        throw CloseConnection("PgMarkupRepository::readAll connection isn't open");
}

void PgMarkupRepository::update(std::string wordform, MarkupUnit &markup)
{
    if (connection && connection->is_open())
    {
        pqxx::work txn{*connection};  

        std::string query(
        "update markupunit set " + markup.toMetaString() + 
        "where wordform = '" + wordform + "';");

        try {
        txn.exec(query);
        } catch (...) {throw BadUpdate("PgMarkupRepository::update exception during update query");}
        txn.commit();
    }
    else
        throw CloseConnection("PgMarkupRepository::update connection isn't open");
}

void PgMarkupRepository::delete_(std::string wordform)
{
    if (connection && connection->is_open())
    {
        pqxx::work txn{*connection};  

        try {
        txn.exec(
        "delete from markupunit "
        "where wordform = '" + wordform + "';");
        } catch (...) {throw BadDelete("PgMarkupRepository::delete_ exception during delete query");}

        txn.commit();
    } 
    else
        throw CloseConnection("PgMarkupRepository::delete_ connection isn't open");
}

std::shared_ptr<MarkupRepository> PgMarkupRepositoryCreator::create()
{
    return std::make_shared<PgMarkupRepository>(connectionString);
}
