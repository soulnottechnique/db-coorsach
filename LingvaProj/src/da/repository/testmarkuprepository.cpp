#include "../repository/testmarkuprepository.h"

void TestMarkupRepository::create(MarkupUnit &markup)
{
    std::cout << "TestMarkupRepository::create" << std::endl;
}

std::vector<MarkupUnit> TestMarkupRepository::read(std::string wordform)
{
    std::cout << "TestMarkupRepository::read, wordform = " << wordform << std::endl;
    return std::vector<MarkupUnit>();
}

std::vector<MarkupUnit> TestMarkupRepository::readAll()
{
    std::cout << "TestMarkupRepository::readAll" << std::endl;
    return std::vector<MarkupUnit>();
}

void TestMarkupRepository::update(std::string wordform, MarkupUnit &markup)
{
    std::cout << "TestMarkupRepository::update, wordform = " << wordform << std::endl;
}

void TestMarkupRepository::delete_(std::string wordform)
{
    std::cout << "TestMarkupRepository::delete_, wordform = " << wordform << std::endl;
}

std::shared_ptr<MarkupRepository> TestMarkupRepositoryCreator::create()
{
    return std::make_shared<TestMarkupRepository>(connectionString);
}

void TestMarkupRepository::create(std::vector<MarkupUnit> mvec)
{
    std::cout << "TestMarkupRepository::create" << std::endl;
}