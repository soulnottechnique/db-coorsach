#pragma once

#include <iostream>
#include <memory>
#include <pqxx/pqxx>
#include "../repository/accountrepository.h"

class PgAccountRepository : public AccountRepository {
public:
    PgAccountRepository(std::string connectionString);
    virtual ~PgAccountRepository() = default;

    virtual void create(Account &account) override;
    virtual Account read(std::string login) override;
    virtual std::vector<Account> readAll() override;
    virtual void update(std::string login, Account &account) override;
    virtual void delete_(std::string login) override;

private:
    std::shared_ptr<pqxx::connection> connection;
};

class PgAccountRepositoryCreator : public AccountRepositoryCreator{
public:
    PgAccountRepositoryCreator(std::string connectionString) : AccountRepositoryCreator(connectionString) {}
    virtual ~PgAccountRepositoryCreator() = default;

    virtual std::shared_ptr<AccountRepository> create() override;
};