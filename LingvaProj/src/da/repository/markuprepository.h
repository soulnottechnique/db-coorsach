#pragma once

#include <vector>
#include "../../core/sysent/markupunit.h"
#include "../exceptions/AccessException.hpp"

class MarkupRepository {
public:
    MarkupRepository(std::string connectionString) : connectionString(connectionString) {}
    virtual ~MarkupRepository() = default;

    virtual void create(MarkupUnit &markup) = 0;
    virtual void create(std::vector<MarkupUnit> mvec) = 0;
    virtual std::vector<MarkupUnit> read(std::string wordform) = 0;
    virtual std::vector<MarkupUnit> readAll() = 0;
    virtual void update(std::string wordform, MarkupUnit &markup) = 0;
    virtual void delete_(std::string wordform) = 0;

protected:
    std::string connectionString;
};

class MarkupRepositoryCreator {
public:
    MarkupRepositoryCreator(std::string connectionString) : connectionString(connectionString) {}
    virtual ~MarkupRepositoryCreator() = default;

    virtual std::shared_ptr<MarkupRepository> create() = 0;

protected:
    std::string connectionString;
};