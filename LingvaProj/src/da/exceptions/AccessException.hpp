#pragma once

#include <exception>
#include <string>
class AccessException : public std::exception {
    private:
    std::string str;
    public:
    AccessException(std::string _str) : str(_str){}

    virtual const char * what() const throw() {return str.c_str();}
};

class CloseConnection : public AccessException {
    private:
    std::string str;
    public:
    CloseConnection(std::string _str) : AccessException(_str){}

    virtual const char * what() const throw() {return str.c_str();}
};

class EmptyResponse : public AccessException {
    private:
    std::string str;
    public:
    EmptyResponse(std::string _str) : AccessException(_str){}

    virtual const char * what() const throw() {return str.c_str();}
};

class BadInsert : public AccessException {
    private:
    std::string str;
    public:
    BadInsert(std::string _str) : AccessException(_str){}

    virtual const char * what() const throw() {return str.c_str();}
};

class BadUpdate : public AccessException {
    private:
    std::string str;
    public:
    BadUpdate(std::string _str) : AccessException(_str){}

    virtual const char * what() const throw() {return str.c_str();}
};

class BadDelete : public AccessException {
    private:
    std::string str;
    public:
    BadDelete(std::string _str) : AccessException(_str){}

    virtual const char * what() const throw() {return str.c_str();}
};