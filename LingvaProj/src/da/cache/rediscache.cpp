#include "rediscache.h"

namespace resps = rediscpp::resp::serialization;
namespace respds = rediscpp::resp::deserialization;

RedisMarkupCache::RedisMarkupCache(std::string port) : MarkupCache()
{
    stream = rediscpp::make_stream("localhost", port);
}

void RedisMarkupCache::put(MarkupUnit &mu)
{
    auto response = rediscpp::execute(*stream, "LPUSH", key, mu.toString());
    cached = response.as<size_t>();
}

std::vector<MarkupUnit> RedisMarkupCache::get()
{
    std::vector<MarkupUnit> mvec;
    while (cached--)
    {
        auto response = rediscpp::execute(*stream, "LPOP", key);
        mvec.push_back(MarkupUnit(response.as<std::string>()));
    }

    cached = 0;
    return mvec;
}

std::shared_ptr<MarkupCache> RedisMarkupCacheCreator::create()
{
    return std::make_shared<RedisMarkupCache>(port);
}