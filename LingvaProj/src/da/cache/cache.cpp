#include "cache.h"

MarkupCache::MarkupCache() : cached(0) {}

bool MarkupCache::empty()
{
    return cached == 0;
}