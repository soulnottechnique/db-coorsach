#pragma once

#include <vector>
#include <string>
#include <memory>
#include "../../core/sysent/markupunit.h"

class MarkupCache {
public:
    MarkupCache();
    virtual ~MarkupCache() = default;

    virtual void put(MarkupUnit &mu) = 0;
    virtual std::vector<MarkupUnit> get() = 0;
    bool empty();

protected:
    size_t cached;
};

class MarkupCacheCreator {
public:
    MarkupCacheCreator(std::string port) : port(port) {}
    virtual ~MarkupCacheCreator() = default;

    virtual std::shared_ptr<MarkupCache> create() = 0;

protected:
    std::string port;
};