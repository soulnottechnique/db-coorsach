#pragma once

#include "cache.h"

#include <iostream>
#include <string>

#include <redis-cpp/stream.h>
#include <redis-cpp/execute.h>

class RedisMarkupCache : public MarkupCache {
public:
    RedisMarkupCache(std::string port);
    virtual ~RedisMarkupCache() = default;

    virtual void put(MarkupUnit &mu) override;
    virtual std::vector<MarkupUnit> get() override;

private:
    const std::string key = "markup";
    std::shared_ptr<std::iostream> stream;
};

class RedisMarkupCacheCreator : public MarkupCacheCreator {
public:
    RedisMarkupCacheCreator(std::string port) : MarkupCacheCreator(port) {}
    virtual ~RedisMarkupCacheCreator() = default;

    virtual std::shared_ptr<MarkupCache> create() override;
};