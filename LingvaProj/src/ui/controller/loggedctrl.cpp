#include "loggedctrl.h"

AdminLoggedCtrl::AdminLoggedCtrl(Account account, std::shared_ptr<Model> model, std::shared_ptr<LoggedView> view) :
    account(account), model(model), view(std::static_pointer_cast<AdminLoggedView>(view)) {}

LinguistLoggedCtrl::LinguistLoggedCtrl(Account account, std::shared_ptr<Model> model, std::shared_ptr<LoggedView> view) :
    account(account), model(model), view(std::static_pointer_cast<LinguistLoggedView>(view)) {}

CommonLoggedCtrl::CommonLoggedCtrl(Account account, std::shared_ptr<Model> model, std::shared_ptr<LoggedView> view) :
    account(account), model(model), view(std::static_pointer_cast<CommonLoggedView>(view)) {}

LoggedCtrlCreator::LoggedCtrlCreator(Account account, std::shared_ptr<Model> model, std::shared_ptr<LoggedView> view) :
    account(account), model(model), view(view) {}

void AdminLoggedCtrl::execute()
{
    bool work = true;
    while (work)
    {
        view->renderMenu();
        lop_t action = view->onActionSelected();

        switch (action)
        {
        case READ_ALL_ACCOUNTS:
            view->onReadAllAccounts();
            try {
                model->execute(std::make_shared<ReadAllAccounts>());
                view->renderAccounts(model->getAllAccounts());
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case READ_ACCOUNT:
            view->renderReadAccount();
            view->onReadAccount();
            try {
                model->execute(std::make_shared<ReadAccount>(view->getLogin()));
                view->renderAccount(model->getAccount());
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case READ_ALL_MARKUP:
            view->onReadAllMarkup();
            try {
                model->execute(std::make_shared<ReadAllMarkup>());
                view->renderAllMarkup(model->getAllMarkup());
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case READ_MARKUP:
            view->renderReadMarkup();
            view->onReadMarkup();
            try {
                model->execute(std::make_shared<ReadMarkup>(view->getWform()));
                view->renderAllMarkup(model->getAllMarkup());
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case ADD_MARKUP:
            view->renderAddMarkup();
            view->onAddMarkup();
            try {
                MarkupUnit mu(view->getWform(), view->getCat(), view->getMorph());
                model->execute(std::make_shared<AddMarkup>(mu));
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case DELETE_ACCOUNT:
            view->renderDeleteAccount();
            view->onDeleteAccount();
            try {
                model->execute(std::make_shared<DeleteAccount>(view->getLogin()));
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case ENABLE_CACHE:
            view->renderEnableCache();
            view->onEnableCache();
            try {
                model->execute(std::make_shared<EnableCache>(view->getStatus()));
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case FLUSH_CACHE:
            try {
                model->execute(std::make_shared<FlushCache>());
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case EXIT:
            work = false;
            break;

        default:
            break;
        }
    }
    
}


void LinguistLoggedCtrl::execute()
{
    bool work = true;
    while (work)
    {
        view->renderMenu();
        lop_t action = view->onActionSelected();

        switch (action)
        {
        case READ_ALL_MARKUP:
            view->onReadAllMarkup();
            try {
                model->execute(std::make_shared<ReadAllMarkup>());
                view->renderAllMarkup(model->getAllMarkup());
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case READ_MARKUP:
            view->renderReadMarkup();
            view->onReadMarkup();
            try {
                model->execute(std::make_shared<ReadMarkup>(view->getWform()));
                view->renderAllMarkup(model->getAllMarkup());
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case ADD_MARKUP:
            view->renderAddMarkup();
            view->onAddMarkup();
            try {
                MarkupUnit mu(view->getWform(), view->getCat(), view->getMorph());
                model->execute(std::make_shared<AddMarkup>(mu));
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case ENABLE_CACHE:
            view->renderEnableCache();
            view->onEnableCache();
            try {
                model->execute(std::make_shared<EnableCache>(view->getStatus()));
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case FLUSH_CACHE:
            try {
                model->execute(std::make_shared<FlushCache>());
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case EXIT:
            work = false;
            break;

        default:
            break;
        }
    }
    
}

void CommonLoggedCtrl::execute()
{
    bool work = true;
    while (work)
    {
        view->renderMenu();
        lop_t action = view->onActionSelected();

        switch (action)
        {
        case READ_ALL_MARKUP:
            view->onReadAllMarkup();
            try {
                model->execute(std::make_shared<ReadAllMarkup>());
                view->renderAllMarkup(model->getAllMarkup());
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case READ_MARKUP:
            view->renderReadMarkup();
            view->onReadMarkup();
            try {
                model->execute(std::make_shared<ReadMarkup>(view->getWform()));
                view->renderAllMarkup(model->getAllMarkup());
            } catch (std::exception &e) {view->renderMessage(e.what());}
            break;

        case EXIT:
            work = false;
            break;

        default:
            break;
        }
    }
    
}

std::shared_ptr<LoggedCtrl> LoggedCtrlCreator::create()
{
    if (account.getRole() == "ROLE_ADMIN")
        return std::make_shared<AdminLoggedCtrl>(account, model, view);
    else if (account.getRole() == "ROLE_LINGUIST")
        return std::make_shared<LinguistLoggedCtrl>(account, model, view);
    else if (account.getRole() == "ROLE_COMMON")
        return std::make_shared<CommonLoggedCtrl>(account, model, view);
    else return nullptr;
}