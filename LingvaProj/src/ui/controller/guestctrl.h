#pragma once 

#include <memory>

#include "../view/guestview.h"
#include "../../core/model/model.h"


class GuestCtrl {
public:
    GuestCtrl(std::shared_ptr<Model> model, std::shared_ptr<GuestView> view);

    Account execute();

private:
    std::shared_ptr<GuestView> view;
    std::shared_ptr<Model> model;
};