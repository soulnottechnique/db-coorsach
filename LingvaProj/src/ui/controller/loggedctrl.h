#pragma once

#include "../../core/model/model.h"
#include "../view/loggedview.h"
#include "../../core/sysent/account.h"

class LoggedCtrl {
public:
    LoggedCtrl() = default; 
    virtual ~LoggedCtrl() = default;

    virtual void execute() = 0;
};

class AdminLoggedCtrl : public LoggedCtrl {
public:
    AdminLoggedCtrl(Account account, std::shared_ptr<Model> model, std::shared_ptr<LoggedView> view);
    virtual ~AdminLoggedCtrl() = default;

    virtual void execute() override;

private:
    Account account;
    std::shared_ptr<Model> model;
    std::shared_ptr<AdminLoggedView> view;
};

class LinguistLoggedCtrl : public LoggedCtrl {
public:
    LinguistLoggedCtrl(Account account, std::shared_ptr<Model> model, std::shared_ptr<LoggedView> view);
    virtual ~LinguistLoggedCtrl() = default;

    virtual void execute() override;

private:
    Account account;
    std::shared_ptr<Model> model;
    std::shared_ptr<LinguistLoggedView> view;
};

class CommonLoggedCtrl : public LoggedCtrl {
public:
    CommonLoggedCtrl(Account account, std::shared_ptr<Model> model, std::shared_ptr<LoggedView> view);
    virtual ~CommonLoggedCtrl() = default;

    virtual void execute() override;

private:
    Account account;
    std::shared_ptr<Model> model;
    std::shared_ptr<CommonLoggedView> view;
};

class LoggedCtrlCreator {
public:
    LoggedCtrlCreator(Account account, std::shared_ptr<Model> model, std::shared_ptr<LoggedView> view);
    ~LoggedCtrlCreator() = default;

    std::shared_ptr<LoggedCtrl> create();

private:
    Account account;
    std::shared_ptr<Model> model;
    std::shared_ptr<LoggedView> view;
};