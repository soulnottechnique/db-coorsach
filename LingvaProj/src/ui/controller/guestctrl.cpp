#include "guestctrl.h"

GuestCtrl::GuestCtrl(std::shared_ptr<Model> model, std::shared_ptr<GuestView> view) : 
    view(view), model(model) {}

Account GuestCtrl::execute()
{
    bool unlog = true;
    while (unlog)
    {
        view->renderMenu();
        op_t action = view->onActionSelected();

        switch (action)
        {
        case LOGIN:
            view->renderLogin();
            view->onLogin();
            
            try{
            model->execute(std::make_shared<LoginAccept>(view->getLogin(), view->getPassword()));
            unlog = false;
            } catch (...) {view->renderMessage("Error");}
            break;

        case REGISTER:
            view->renderRegister();
            view->onRegister();
            
            try {
            Account a(view->getLogin(), view->getMail(), view->getName(), view->getPassword(), view->getRole());
            model->execute(std::make_shared<AddAccount>(a));
            } catch (...) {view->renderMessage("Error");}
            break;

        default:
            break;
        }
    }

    return model->getAccount();
}