#pragma once

#include <string>
#include "../../core/model/model.h"

typedef enum {
    READ_ALL_ACCOUNTS, READ_ACCOUNT, READ_ALL_MARKUP, 
    READ_MARKUP, ADD_MARKUP, DELETE_ACCOUNT, 
    ENABLE_CACHE, FLUSH_CACHE, 
    EXIT} lop_t;

class LoggedView {
public:
    LoggedView() = default;
    virtual ~LoggedView() = default;

    std::string getWform() {return wform;}
    std::string getLogin() {return login;}
    std::string getMorph() {return morph;}
    std::string getCat() {return cat;}
    bool getStatus() {return status;}

protected:
    std::string wform, login, morph, cat;
    bool status;
};

class AdminLoggedView : public LoggedView {
public:
    AdminLoggedView() = default;
    virtual ~AdminLoggedView() = default;

    virtual void renderMenu() = 0;
    virtual void renderMessage(std::string msg) = 0;
    virtual void renderReadAccount() = 0;
    virtual void renderReadMarkup() = 0;
    virtual void renderAddMarkup() = 0;
    virtual void renderDeleteAccount() = 0;
    virtual void renderEnableCache() = 0;

    virtual void renderAccounts(std::vector<Account> accv) = 0;
    virtual void renderAccount(Account acc) = 0;
    virtual void renderAllMarkup(std::vector<MarkupUnit> muv) = 0;
    virtual void renderMarkup(MarkupUnit mu) = 0;

    virtual lop_t onActionSelected() = 0;
    virtual void onReadAllAccounts() = 0;
    virtual void onReadAccount() = 0;
    virtual void onReadAllMarkup() = 0;
    virtual void onReadMarkup() = 0;
    virtual void onAddMarkup() = 0;
    virtual void onDeleteAccount() = 0;
    virtual void onEnableCache() = 0;
};

class LinguistLoggedView : public LoggedView {
public:
    LinguistLoggedView() = default;
    virtual ~LinguistLoggedView() = default;

    virtual void renderMenu() = 0;
    virtual void renderMessage(std::string msg) = 0;
    virtual void renderReadMarkup() = 0;
    virtual void renderAddMarkup() = 0;
    virtual void renderEnableCache() = 0;

    virtual void renderAllMarkup(std::vector<MarkupUnit> muv) = 0;
    virtual void renderMarkup(MarkupUnit mu) = 0;

    virtual lop_t onActionSelected() = 0;
    virtual void onReadAllMarkup() = 0;
    virtual void onReadMarkup() = 0;
    virtual void onAddMarkup() = 0;
    virtual void onEnableCache() = 0;
};

class CommonLoggedView : public LoggedView {
public:
    CommonLoggedView() = default;
    virtual ~CommonLoggedView() = default;

    virtual void renderMenu() = 0;
    virtual void renderMessage(std::string msg) = 0;
    virtual void renderReadMarkup() = 0;

    virtual void renderAllMarkup(std::vector<MarkupUnit> muv) = 0;
    virtual void renderMarkup(MarkupUnit mu) = 0;

    virtual lop_t onActionSelected() = 0;
    virtual void onReadAllMarkup() = 0;
    virtual void onReadMarkup() = 0;
};