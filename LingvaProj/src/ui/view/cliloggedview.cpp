#include "cliloggedview.h"

//ypedef enum {READ_ALL_ACCOUNTS, READ_ACCOUNT, READ_ALL_MARKUP, READ_MARKUP, ADD_MARKUP, DELETE_ACCOUNT, EXIT} op_t;


void CLIAdminLoggedView::renderMenu()
{
    std::cout << "0 - read all accounts\n1 - read account\n2 - read all markup\n3 - read markup\n4 - add markup\n5 - delete account\n6 - enable cache\n7 - flush cache\n8 - exit" << std::endl;
}

void CLIAdminLoggedView::renderMessage(std::string msg)
{
    std::cout << msg << std::endl;
}

void CLIAdminLoggedView::renderReadAccount()
{
    std::cout << "enter login" << std::endl;
}

void CLIAdminLoggedView::renderReadMarkup()
{
    std::cout << "enter wordform" << std::endl;
}

void CLIAdminLoggedView::renderAddMarkup()
{
    std::cout << "enter wordform\ncategory\nmorphology" << std::endl;
}

void CLIAdminLoggedView::renderDeleteAccount()
{
    std::cout << "enter login" << std::endl;
}

void CLIAdminLoggedView::renderAccounts(std::vector<Account> accv)
{
    for (auto acc : accv)
        std::cout << acc.toMetaString() << std::endl;
}

void CLIAdminLoggedView::renderAccount(Account acc)
{
    std::cout << acc.toMetaString() << std::endl;
}

void CLIAdminLoggedView::renderAllMarkup(std::vector<MarkupUnit> muv) 
{
    for (auto mu : muv)
        std::cout << mu.toMetaString() << std::endl;
}

void CLIAdminLoggedView::renderMarkup(MarkupUnit mu)
{
    std::cout << mu.toMetaString() << std::endl;
}

lop_t CLIAdminLoggedView::onActionSelected()
{
    int action;
    std::cin >> action;
    return (lop_t)action;
}

void CLIAdminLoggedView::onReadAllAccounts()
{

}

void CLIAdminLoggedView::onReadAccount()
{
    std::cin >> login;
}

void CLIAdminLoggedView::onReadAllMarkup()
{

}

void CLIAdminLoggedView::onReadMarkup()
{
    std::cin >> wform;
}

void CLIAdminLoggedView::onAddMarkup()
{
    std::cin >> wform >> cat >> morph;
}

void CLIAdminLoggedView::onDeleteAccount()
{
    std::cin >> login;
}


void CLILinguistLoggedView::renderMenu()
{
    std::cout << "2 - read all markup\n3 - read markup\n4 - add markup\n6 - enable cache\n7 - flush cache\n8 - exit" << std::endl;
}

void CLILinguistLoggedView::renderMessage(std::string msg)
{
    std::cout << msg << std::endl;
}

void CLILinguistLoggedView::renderReadMarkup()
{
    std::cout << "enter wordform" << std::endl;
}

void CLILinguistLoggedView::renderAddMarkup()
{
    std::cout << "enter wordform\ncategory\nmorphology" << std::endl;
}

void CLILinguistLoggedView::renderAllMarkup(std::vector<MarkupUnit> muv) 
{
    for (auto mu : muv)
        std::cout << mu.toMetaString() << std::endl;
}

void CLILinguistLoggedView::renderMarkup(MarkupUnit mu)
{
    std::cout << mu.toMetaString() << std::endl;
}

lop_t CLILinguistLoggedView::onActionSelected()
{
    int action;
    std::cin >> action;
    return (lop_t)action;
}

void CLILinguistLoggedView::onReadAllMarkup()
{

}

void CLILinguistLoggedView::onReadMarkup()
{
    std::cin >> wform;
}

void CLILinguistLoggedView::onAddMarkup()
{
    std::cin >> wform >> cat >> morph;
}


void CLICommonLoggedView::renderMenu()
{
    std::cout << "2 - read all markup\n3 - read markup\n6 - exit" << std::endl;
}

void CLICommonLoggedView::renderMessage(std::string msg)
{
    std::cout << msg << std::endl;
}

void CLICommonLoggedView::renderReadMarkup()
{
    std::cout << "enter wordform" << std::endl;
}

void CLICommonLoggedView::renderAllMarkup(std::vector<MarkupUnit> muv) 
{
    for (auto mu : muv)
        std::cout << mu.toMetaString() << std::endl;
}

void CLICommonLoggedView::renderMarkup(MarkupUnit mu)
{
    std::cout << mu.toMetaString() << std::endl;
}

lop_t CLICommonLoggedView::onActionSelected()
{
    int action;
    std::cin >> action;
    return (lop_t)action;
}

void CLICommonLoggedView::onReadAllMarkup()
{

}

void CLICommonLoggedView::onReadMarkup()
{
    std::cin >> wform;
}

void CLIAdminLoggedView::renderEnableCache()
{
    std::cout << "Enable cache? 0/1 : " << std::endl;
}

void CLILinguistLoggedView::renderEnableCache()
{
    std::cout << "Enable cache? 0/1 : " << std::endl;
}

void CLIAdminLoggedView::onEnableCache()
{
    std::cin >> status;
}

void CLILinguistLoggedView::onEnableCache()
{
    std::cin >> status;
}

std::shared_ptr<LoggedView> CLILoggedViewCreator::create()
{
    if (account.getRole() == "ROLE_ADMIN")
        return std::make_shared<CLIAdminLoggedView>();
    else if (account.getRole() == "ROLE_LINGUIST")
        return std::make_shared<CLILinguistLoggedView>();
    else if (account.getRole() == "ROLE_COMMON")
        return std::make_shared<CLICommonLoggedView>();
    else return nullptr;
}