#pragma once

#include <iostream>

#include "guestview.h"

class CLIGuestView : public GuestView {
public:
    CLIGuestView() = default;
    ~CLIGuestView() = default;

    virtual void renderLogin() override;
    virtual void renderRegister() override;
    virtual void renderMenu() override;
    virtual void renderMessage(std::string msg) override;

    virtual op_t onActionSelected() override;
    virtual void onLogin() override;
    virtual void onRegister() override;
};