#pragma once

typedef enum {LOGIN, REGISTER} op_t;

class GuestView {
public:
    GuestView() = default;
    ~GuestView() = default;

    virtual void renderLogin() = 0;
    virtual void renderRegister() = 0;
    virtual void renderMenu() = 0;

    virtual op_t onActionSelected() = 0;
    virtual void onLogin() = 0;
    virtual void onRegister() = 0;
    virtual void renderMessage(std::string msg) = 0;

    std::string getName() {return name;}
    std::string getLogin() {return login;}
    std::string getPassword() {return password;}
    std::string getMail() {return mail;}
    std::string getRole() {return role;}

protected:
    std::string name, login, password, mail, role;
};