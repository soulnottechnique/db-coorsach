cmake_minimum_required(VERSION 3.8)

project(view)	

add_library(view SHARED 
    guestview.h
    cliguestview.h
    cliguestview.cpp
    loggedview.h
    cliloggedview.h
    cliloggedview.cpp
)

set_target_properties(view PROPERTIES LINKER_LANGUAGE CXX)