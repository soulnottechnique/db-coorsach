#pragma once

#include <iostream>

#include "loggedview.h"

class CLIAdminLoggedView : public AdminLoggedView {
public:
    CLIAdminLoggedView() = default;
    virtual ~CLIAdminLoggedView() = default;

    virtual void renderMenu() override;
    virtual void renderMessage(std::string msg) override;
    virtual void renderReadAccount() override;
    virtual void renderReadMarkup() override;
    virtual void renderAddMarkup() override;
    virtual void renderDeleteAccount() override;
    virtual void renderEnableCache() override;

    virtual void renderAccounts(std::vector<Account> accv) override;
    virtual void renderAccount(Account acc) override;
    virtual void renderAllMarkup(std::vector<MarkupUnit> muv) override;
    virtual void renderMarkup(MarkupUnit mu) override;

    virtual lop_t onActionSelected() override;
    virtual void onReadAllAccounts() override;
    virtual void onReadAccount() override;
    virtual void onReadAllMarkup() override;
    virtual void onReadMarkup() override;
    virtual void onAddMarkup() override;
    virtual void onDeleteAccount() override;
    virtual void onEnableCache() override;
};

class CLILinguistLoggedView : public LinguistLoggedView {
public:
    CLILinguistLoggedView() = default;
    virtual ~CLILinguistLoggedView() = default;

    virtual void renderMenu() override;
    virtual void renderMessage(std::string msg) override;
    virtual void renderReadMarkup() override;
    virtual void renderAddMarkup() override;
    virtual void renderEnableCache() override;

    virtual void renderAllMarkup(std::vector<MarkupUnit> muv) override;
    virtual void renderMarkup(MarkupUnit mu) override;

    virtual lop_t onActionSelected() override;
    virtual void onReadAllMarkup() override;
    virtual void onReadMarkup() override;
    virtual void onAddMarkup() override;
    virtual void onEnableCache() override;
};

class CLICommonLoggedView : public CommonLoggedView {
public:
    CLICommonLoggedView() = default;
    virtual ~CLICommonLoggedView() = default;

    virtual void renderMenu() override;
    virtual void renderMessage(std::string msg) override;
    virtual void renderReadMarkup() override;

    virtual void renderAllMarkup(std::vector<MarkupUnit> muv) override;
    virtual void renderMarkup(MarkupUnit mu) override;

    virtual lop_t onActionSelected() override;
    virtual void onReadAllMarkup() override;
    virtual void onReadMarkup() override;
};

class CLILoggedViewCreator {
public:
    CLILoggedViewCreator(Account a) : account(a) {}
    ~CLILoggedViewCreator() = default;

    std::shared_ptr<LoggedView> create();

private:
    Account account;
};