/*
#include <iostream>
#include "core/model/model.h"
#include "ui/controller/guestctrl.h"
#include "ui/controller/loggedctrl.h"
#include "ui/view/cliguestview.h"
#include "ui/view/cliloggedview.h"

int main(void)
{
    std::shared_ptr<BaseConfig> conf = std::make_shared<Config>("../conf/conf");
    std::shared_ptr<Model> model = std::make_shared<Model>(conf);

    std::shared_ptr<GuestView> guestView = std::make_shared<CLIGuestView>();
    GuestCtrl guestConstroller(model, guestView);
    
    Account account = guestConstroller.execute();
    
    CLILoggedViewCreator viewCreator(account);
    std::shared_ptr<LoggedView> loggedView = viewCreator.create();

    LoggedCtrlCreator controllerCreator(account, model, loggedView);
    std::shared_ptr<LoggedCtrl> loggedController = controllerCreator.create();

    loggedController->execute();

    return 0;
}
*/

#include "core/model/model.h"
#include "ui/form/loginform.hpp"
#include "ui/form/adminform.hpp"

int main(void)
{
    std::shared_ptr<Account> acc = nullptr;

    std::shared_ptr<BaseConfig> conf = std::make_shared<Config>("../conf/conf");
    std::shared_ptr<Model> model = std::make_shared<Model>(conf);

    application::run(LoginForm(model, acc));
    if (acc)
    {
        application::run(AdminForm(model, acc));
    }
}