#ifndef COMMONWINDOW_H
#define COMMONWINDOW_H

#include <QWidget>
#include <QMessageBox>
#include <QJsonObject>
#include <QJsonDocument>
#include "src/core/model/model.h"

namespace Ui {
class commonwindow;
}

class commonwindow : public QWidget
{
    Q_OBJECT

public:
    explicit commonwindow(std::shared_ptr<Model> model, QWidget *parent = nullptr);
    ~commonwindow();

    void setAcc(std::shared_ptr<Account> acc) {this->acc = acc;}

signals:
    void closedSig();

private slots:
    virtual void closeEvent(QCloseEvent *event) override;

    void on_searchButton_2_clicked();

    void on_searchAllButton_clicked();

    void on_read_clearButton_clicked();

private:

    Ui::commonwindow *ui;
    std::shared_ptr<Model> model;
    std::shared_ptr<Account> acc;
};

#endif // COMMONWINDOW_H
