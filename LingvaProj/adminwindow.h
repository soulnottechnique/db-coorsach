#ifndef ADMINWINDOW_H
#define ADMINWINDOW_H

#include <QWidget>
#include <QMessageBox>
#include <QJsonObject>
#include <QJsonDocument>
#include "src/core/model/model.h"

namespace Ui {
class AdminWindow;
}

class AdminWindow : public QWidget
{
    Q_OBJECT

public:
    explicit AdminWindow(std::shared_ptr<Model> model, QWidget *parent = nullptr);
    ~AdminWindow();

    void setAcc(std::shared_ptr<Account> acc) {this->acc = acc;}

signals:
    void closedSig();

private slots:
    void on_searchButton_clicked();

    virtual void closeEvent(QCloseEvent *event) override;

    void on_deleteButton_clicked();

    void on_pushButton_clicked();

    void on_checkBox_stateChanged(int arg1);

    void on_searchButton_2_clicked();

    void on_searchAllButton_clicked();

    void on_allAccountsButton_clicked();

    void on_addButton_clicked();

    void on_adj_const_currentIndexChanged(int index);

    void on_num_countRadio_toggled(bool checked);

    void on_pronoun_categoryBox_currentIndexChanged(int index);

    void on_participle_pledgeBox_currentIndexChanged(int index);

    void on_participle_fullformRadio_toggled(bool checked);

    void on_read_clearButton_clicked();

    void on_account_clearButton_clicked();

private:
    void add_noun();
    void add_adjective();
    void add_numeral();
    void add_pronoun();
    void add_verb();
    void add_participle();
    void add_adv_participle();
    void add_adverb();
    void add_preposition();
    void add_conjunction();
    void add_particle();
    void add_interjection();
    void add(QJsonObject &json);
    std::string read_category();

    Ui::AdminWindow *ui;
    std::shared_ptr<Model> model;
    std::shared_ptr<Account> acc;
};

#endif // ADMINWINDOW_H
