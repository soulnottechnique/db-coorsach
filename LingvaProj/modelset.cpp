#include "modelset.h"

ModelSet::ModelSet(std::string filename)
{
    std::shared_ptr<BaseConfig> ac = std::make_shared<Config>("../conf/conf");
    std::shared_ptr<BaseConfig> lc = std::make_shared<Config>("../conf/conf", "ROLE_LINGUIST");
    std::shared_ptr<BaseConfig> cc = std::make_shared<Config>("../conf/conf", "ROLE_COMMON");

    adminModel = std::make_shared<Model>(ac);
    lingvaModel = std::make_shared<Model>(lc);
    commonModel = std::make_shared<Model>(cc);
}
