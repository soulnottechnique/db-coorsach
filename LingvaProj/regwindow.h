#ifndef REGWINDOW_H
#define REGWINDOW_H

#include <QMessageBox>
#include <QWidget>
#include "src/core/model/model.h"

namespace Ui {
class RegWindow;
}

class RegWindow : public QWidget
{
    Q_OBJECT

public:
    explicit RegWindow(std::shared_ptr<Model> model, QWidget *parent = nullptr);
    ~RegWindow();

signals:
    void closedSig();

private slots:
    void on_regButton_clicked();

    void on_loginLine_textChanged(const QString &arg1);

    virtual void closeEvent(QCloseEvent *event) override;

private:
    Ui::RegWindow *ui;
    std::shared_ptr<Model> model;
};

#endif // REGWINDOW_H
