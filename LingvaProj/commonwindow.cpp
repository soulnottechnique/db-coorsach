#include "commonwindow.h"
#include "ui_commonwindow.h"

commonwindow::commonwindow(std::shared_ptr<Model> model, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::commonwindow),
    model(model)
{
    ui->setupUi(this);
}

void commonwindow::closeEvent(QCloseEvent *event)
{
    emit closedSig();
}

commonwindow::~commonwindow()
{
    delete ui;
}

void commonwindow::on_searchButton_2_clicked()
{
    try {
        std::string wf = ui->lineEdit->text().toStdString();
        model->execute(std::make_shared<ReadMarkup>(wf));
        std::vector<MarkupUnit> mvec = model->getAllMarkup();
        if (mvec.empty())
            QMessageBox::information(this, "Информация", "В базе отсутствует данная словоформа");
        else
        {
            ui->MarkupTextBrowser->append(QString(acc->getLogin().c_str()) + ">>> " + ui->lineEdit->text() + "\n");
            for (auto m : mvec)
                ui->MarkupTextBrowser->append(QString(m.toMetaString().c_str()) + "\n");
        }
    } catch (std::exception &e) {QMessageBox::critical(this, "Ошибка", "Не удалось найти словоформу");}
}


void commonwindow::on_searchAllButton_clicked()
{
    try {
        model->execute(std::make_shared<ReadAllMarkup>());
        std::vector<MarkupUnit> mvec = model->getAllMarkup();
        if (mvec.empty())
            QMessageBox::information(this, "Информация", "База пуста");
        else
        {
            ui->MarkupTextBrowser->append(QString(acc->getLogin().c_str()) + ">>> полный список\n");
            for (auto m : mvec)
                ui->MarkupTextBrowser->append(QString(m.toMetaString().c_str()) + "\n");
        }
    } catch (std::exception &e) {QMessageBox::critical(this, "Ошибка", "Не удалось прочитать словоформы");}
}


void commonwindow::on_read_clearButton_clicked()
{
    ui->MarkupTextBrowser->clear();
}

