#include "loginwindow.h"
#include "./ui_loginwindow.h"

LoginWindow::LoginWindow(std::shared_ptr<ModelSet> mset, QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::LoginWindow)
    , model(mset->getAdminModel())
{
    ui->setupUi(this);
    rw = std::make_shared<RegWindow>(model);
    aw = std::make_shared<AdminWindow>(model);
    cw = std::make_shared<commonwindow>(mset->getCommonModel());
    lw = std::make_shared<lingvawindow>(mset->getLingvaModel());

    QObject::connect(&*rw, SIGNAL(closedSig()), this, SLOT(show()));
    QObject::connect(&*aw, SIGNAL(closedSig()), this, SLOT(show()));
    QObject::connect(&*cw, SIGNAL(closedSig()), this, SLOT(show()));
    QObject::connect(&*lw, SIGNAL(closedSig()), this, SLOT(show()));
}

LoginWindow::~LoginWindow()
{
    delete ui;
}


void LoginWindow::on_logButton_clicked()
{
    try {
        model->execute(std::make_shared<LoginAccept>(ui->loginLine->text().toStdString(), ui->passLine->text().toStdString()));
        if (model->getLoginAccept())
        {
            acc = std::make_shared<Account>(model->getAccount());
            std::string role = acc->getRole();

            if (role == "ROLE_ADMIN")
            {
                aw->setAcc(acc);
                aw->show();
            }
            else if (role == "ROLE_COMMON")
            {
                cw->setAcc(acc);
                cw->show();
            }
            else
            {
                lw->setAcc(acc);
                lw->show();
            }
            this->close();
        }
        else
            QMessageBox::information(this, "Ошибка", "Неправильный логин или пароль");
    } catch (...) {QMessageBox::critical(this, "Ошибка", "Невозможно подключиться к серверу");}
}


void LoginWindow::on_regButton_clicked()
{
    rw->show();
    this->close();
}

