#ifndef LOGINWINDOW_H
#define LOGINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>

#include "src/core/model/model.h"
#include "regwindow.h"
#include "adminwindow.h"
#include "commonwindow.h"
#include "lingvawindow.h"
#include "modelset.h"

QT_BEGIN_NAMESPACE
namespace Ui { class LoginWindow; }
QT_END_NAMESPACE

class LoginWindow : public QMainWindow
{
    Q_OBJECT

public:
    LoginWindow(std::shared_ptr<ModelSet> mset, QWidget *parent = nullptr);
    ~LoginWindow();

private slots:
    void on_logButton_clicked();

    void on_regButton_clicked();

private:
    Ui::LoginWindow *ui;
    std::shared_ptr<Model> model;
    std::shared_ptr<Account> acc;
    std::shared_ptr<RegWindow> rw;
    std::shared_ptr<AdminWindow> aw;
    std::shared_ptr<commonwindow> cw;
    std::shared_ptr<lingvawindow> lw;
};
#endif // LOGINWINDOW_H
