#include <iostream>
#include "../src/repository/pgaccountrepository.h"
#include "../src/repository/pgmarkuprepository.h"

int main(void)
{
    PgAccountRepository accountRepo("postgresql://egor:7856@localhost:5432/markup");
    PgMarkupRepository markupRepo("postgresql://egor:7856@localhost:5432/markup");

    Account a = Account("testlog", "testmail", "name", "parol", "ROLE_ADMIN"), 
    b = Account("testlog", "testmail_new", "name_new", "parol_new", "ROLE_COMMON");
    Category cat("123");
    Morphology mor("1234"), mor1("12345mm");
    std::string name = std::string("wfstring");
    MarkupUnit m(name, cat, mor), m1(name, cat, mor1);

    std::cout << m.toMetaString() << std::endl;

    std::cout << "CREATE/READ ACCOUNT" << std::endl;
    accountRepo.create(a);
    std::cout << accountRepo.read("testlog").toMetaString() << std::endl;

    std::cout << "READ ALL ACCOUNT" << std::endl;
    std::vector<Account> accv = accountRepo.readAll();
    for (auto row : accv) std::cout << row.toMetaString() << std::endl;

    std::cout << "UPDATE/READ ACCOUNT" << std::endl;
    accountRepo.update("testlog", b);
    std::cout << accountRepo.read("testlog").toMetaString() << std::endl;

    std::cout << "DELETE/READ ALL ACCOUNT" << std::endl;
    accountRepo.delete_("testlog");
    accv = accountRepo.readAll();
    for (auto row : accv) std::cout << row.toMetaString() << std::endl;
    
    std::cout << "CREATE/READ MARKUP" << std::endl;
    markupRepo.create(m);
    std::vector<MarkupUnit> accvm = markupRepo.read(name);
    for (auto row : accvm) std::cout << row.toMetaString() << std::endl;

    std::cout << "READ ALL MARKUP" << std::endl;
    accvm = markupRepo.readAll();
    for (auto row : accvm) std::cout << row.toMetaString() << std::endl;

    std::cout << "UPDATE/READ MARKUP" << std::endl;
    markupRepo.update(name, m1);
    accvm = markupRepo.read(name);
    for (auto row : accvm) std::cout << row.toMetaString() << std::endl;

    std::cout << "DELETE/READ ALL MARKUP" << std::endl;
    markupRepo.delete_(name);
    accvm = markupRepo.readAll();
    for (auto row : accvm) std::cout << row.toMetaString() << std::endl;
    
    return 0;
}